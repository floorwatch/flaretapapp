//
//  RestClient.swift
//  FlareTap
//
//  Created by ijones on 11/5/15.
//  Copyright © 2015 Floorwatch. All rights reserved.
//
import Alamofire

protocol RestAPIManagerDelegate: class {
    func restAPIManger(manger: RestAPIManager, didReturnResult result: Any)
}

class RestAPIManager {
    var host : String = ""
    weak var delegate: RestAPIManagerDelegate?
    
    init (host: String) {
        self.host = host
    }
    
    func getStoreList(host: String, parameters: Dictionary<String,String>?) {
        let localhost = "http://52.88.254.252/rest/store/nearest/41.951623/-70.716443/1.5"
        Alamofire.request(.GET, localhost).responseArray { (response: Response<[Store], NSError>) in
        
            let storesArray = response.result.value
            
            if let storesArray = storesArray {
                for store in storesArray {
                    print(store.description)
                }
                print("Before delegate")
                self.delegate?.restAPIManger(self, didReturnResult: storesArray)
                print("After delegate")
            }
        }
    }
    
    
    
    func put() {
        print("put")
        let parameters = [
            "id": "10",
            "content": "something"
        ]
        
        Alamofire.request(.PUT, host, parameters: parameters)
    }
    
    func post() {
        print("post")
        let parameters = [
            "id": "10",
            "content": "something"
        ]
        
        Alamofire.request(.POST, "http://localhost:8080/greeting", parameters: parameters)
    }
    
    func delete() {
        print("delete")
        let parameters = [
            "id": "10",
            "content": "something"
        ]
        
        Alamofire.request(.DELETE, "http://localhost:8080/greeting", parameters: parameters)
    }
}
//
//  Store.swift
//  SidebarMenu
//
//  Created by ijones on 11/4/15.
//  Copyright © 2015 AppCoda. All rights reserved.
//

import MapKit
import Contacts
import ObjectMapper

class Store: NSObject, Mappable, MKAnnotation {
    var title: String?
    var address1: String?
    var address2: String?
    var city: String?
    var country: String?
    var storeName: String?
    var distance: Double?
    var id: Int?
    var latitude: Double?
    var longitude: Double?
    var postalCode: Int?
    var state:String?
    var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        address1 <- map["address1"]
        address2 <- map["address2"]
        city <- map["city"]
        country <- map["country"]
        storeName <- map["description"]
        title <- map["description"]
        distance <- map["distance"]
        id <- map["id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        postalCode <- map["postalCode"]
        state <- map["state"]
        coordinate = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
    }
    
    var subtitle: String? {
        return title
    }
    
    // annotation callout opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDict = [String(CNPostalAddressStreetKey): self.subtitle!]
        let placemark = MKPlacemark(coordinate: self.coordinate, addressDictionary: addressDict)
        
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.title
        
        return mapItem
    }
}



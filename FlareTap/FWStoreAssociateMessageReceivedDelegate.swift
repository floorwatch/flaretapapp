//
//  FWStoreAssociateMessageReceivedDelegate
//  FWCentral
//
//  Created by Greg Mirabito on 1/5/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

protocol FWStoreAssociateMessageReceivedDelegate{
    func didReceiveHelpRequestAcknowlegement(peripheral:CBPeripheral)
    func didReceiveSessionTextAcknowlegement(peripgeral:CBPeripheral)
    func didReceiveSessionText(text:FWSessionText, peripheral:CBPeripheral)
    func didDisconnect(peripheral:CBPeripheral)
}

//
//  FWManagerAdvertisement.swift
//  FWCentral
//
//  Created by Greg Mirabito on 1/5/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

class FWStoreAssociateAdvertisement:NSObject{
    var peripheral:CBPeripheral
    var storeName:String?
    var managerName:String?
    var departmentName:String?
    var storeImage:UIImage?
    var advertisementDate:NSDate
    var RSSI:NSNumber
    
    init(peripheral:CBPeripheral, storeName:String, RSSI:NSNumber) {
        self.peripheral = peripheral
        self.storeName = storeName
        advertisementDate = NSDate()
        self.RSSI = RSSI
    }
}

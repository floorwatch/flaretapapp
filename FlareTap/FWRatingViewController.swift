//
//  FWRatingViewController.swift
//  FlareTapClient
//
//  Created by Greg Mirabito on 12/29/15.
//  Copyright © 2015 FloorWatch. All rights reserved.
//

import UIKit


class FWRatingViewController: UIViewController, UITextViewDelegate {
    var rating:Int = 0
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var comments: UITextView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    
    override func viewDidLoad() {
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        comments.layer.borderWidth = 0.5
        comments.layer.borderColor = borderColor.CGColor
        comments.layer.cornerRadius = 5.0
        submitButton.enabled = false
    }
    @IBAction func submitButtonPressed(sender: AnyObject) {
        
        //todo: submit rating and any comments to webservice
        performSegueWithIdentifier("unwindToAvailableManagersSegue", sender: self)
    }
    @IBAction func cancelButtonPressed(sender: AnyObject) {
    }
    
   
    @IBAction func starSelected(sender: UITapGestureRecognizer) {
        print("star pressed")
        switch sender.view!{
        case star1!:
            rating = 1
            star1.image = UIImage(named: "selectedStar.png")
            star2.image = UIImage(named: "unselectedStar.png")
            star3.image = UIImage(named: "unselectedStar.png")
            star4.image = UIImage(named: "unselectedStar.png")
            star5.image = UIImage(named: "unselectedStar.png")
        case star2!:
            rating = 2
            star1.image = UIImage(named: "selectedStar.png")
            star2.image = UIImage(named: "selectedStar.png")
            star3.image = UIImage(named: "unselectedStar.png")
            star4.image = UIImage(named: "unselectedStar.png")
            star5.image = UIImage(named: "unselectedStar.png")
        case star3!:
            rating = 3
            star1.image = UIImage(named: "selectedStar.png")
            star2.image = UIImage(named: "selectedStar.png")
            star3.image = UIImage(named: "selectedStar.png")
            star4.image = UIImage(named: "unselectedStar.png")
            star5.image = UIImage(named: "unselectedStar.png")
        case star4!:
            rating = 4
            star1.image = UIImage(named: "selectedStar.png")
            star2.image = UIImage(named: "selectedStar.png")
            star3.image = UIImage(named: "selectedStar.png")
            star4.image = UIImage(named: "selectedStar.png")
            star5.image = UIImage(named: "unselectedStar.png")
        case star5!:
            rating = 5
            star1.image = UIImage(named: "selectedStar.png")
            star2.image = UIImage(named: "selectedStar.png")
            star3.image = UIImage(named: "selectedStar.png")
            star4.image = UIImage(named: "selectedStar.png")
            star5.image = UIImage(named: "selectedStar.png")
        default:
            return
        }
        submitButton.enabled = true
        print("rating: \(rating)")
    }
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 140
        let currentString: NSString = textView.text
        let newString: NSString =
        currentString.stringByReplacingCharactersInRange(range, withString: text)
        return newString.length <= maxLength
    }
}

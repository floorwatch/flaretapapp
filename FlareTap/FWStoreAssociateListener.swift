//
//  FWStoreAssociateListener
//  FWCentral
//
//  Created by Greg Mirabito on 1/27/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

protocol FWStoreAssociateListener{
    func didReceiveAdvertisement(advertisement:FWStoreAssociateAdvertisement)
    func didConnectToStoreAssociate(peripheral:CBPeripheral)
    func didFailToConnectToStoreAssociate()
}
//
//  FWClientMessageReceivedDelegate
//  FWPeripheral
//
//  Created by Greg Mirabito on 1/5/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

protocol FWClientMessageReceivedDelegate{
    func didReceiveHelpRequest(helpRequest:FWHelpRequest!, client:CBCentral!)
    func didReceiveSessionText(text:FWSessionText!, client:CBCentral)
    //func helpRequestCancelledByClient(client:CBCentral)
    //func helpRequestTimeExpired(client:CBCentral)
    //func didReceiveUnexpectedDisconnect(client:CBCentral)
}

//
//  AvailableManagersTableTableViewController.swift
//  FloorwatchClient
//
//  Created by Greg Mirabito on 10/7/15.
//  Copyright © 2015 Onset. All rights reserved.
//

import UIKit
import CoreBluetooth

class AvailableManagersTableTableViewController: UITableViewController, FWStoreAssociateListener {
    var centralManager : FWCentralManager?
    var ads = [FWStoreAssociateAdvertisement]()
    var adExpirationTimer:NSTimer?
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55.0
        centralManager = FWCentralManager(storeAssociateListener: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        ads.removeAll()
        tableView.reloadData()
        centralManager?.startScanningForStoreManagers()
        adExpirationTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector:"removeExpiredAdvertisements" , userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        adExpirationTimer?.invalidate()
        centralManager?.stopScanningForStoreManagers()
        
    }
    
    func removeExpiredAdvertisements(){
        //advertisements that have not been received within x seconds should be removed.
        //filter and only keep ads that were created less than x seconds ago
        let oldCount = ads.count
        ads = ads.filter({NSDate().timeIntervalSinceDate($0.advertisementDate) < 8});
        if(oldCount != ads.count){
            self.tableView.reloadData()
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return ads.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FWStoreAssociateCell = tableView.dequeueReusableCellWithIdentifier("associateCell", forIndexPath: indexPath) as! FWStoreAssociateCell
        cell.advertisement = ads[indexPath.row]
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "\(ads.count) associates avalable"
    }
    
    func didReceiveAdvertisement(advertisement: FWStoreAssociateAdvertisement) {
        var oldAd:FWStoreAssociateAdvertisement?
        
        var oldAdIndex : NSNumber? = nil
        for (index, ad) in ads.enumerate() {
            if(ad.peripheral.identifier == advertisement.peripheral.identifier){
                oldAd = ad
                oldAdIndex = NSNumber(integer: index)
                break
            }
        }
        
        if(oldAdIndex != nil){
            ads[oldAdIndex!.integerValue] = advertisement
            if(oldAd == nil || oldAd!.RSSI.integerValue != advertisement.RSSI.integerValue){
                self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: oldAdIndex!.integerValue, inSection: 0)], withRowAnimation: UITableViewRowAnimation.None)
            }
        }else{
            ads.append(advertisement)
            self.tableView.reloadData()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let ad = ads[indexPath.row]
        centralManager?.stopScanningForStoreManagers()
        centralManager?.connectToStoreManager(ad.peripheral)
    }

    
    func didConnectToStoreAssociate(peripheral: CBPeripheral) {
        performSegueWithIdentifier("helpSessionSegue", sender: peripheral)
    }
    
    func didFailToConnectToStoreAssociate() {
        let alert = UIAlertController(title: "Connection failed", message: "There was a problem connecting to the store associate, please try again.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
        centralManager?.startScanningForStoreManagers()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let sessionViewController = segue.destinationViewController as! FWHelpSessionViewController
        sessionViewController.peripheral = sender as? CBPeripheral
        sessionViewController.centralManager = centralManager
        centralManager?.fwStoreAssociateMessageReceivedDelegate = sessionViewController
    }
    
    func refresh(){
        dispatch_async(dispatch_get_main_queue(), {
            [unowned self] in
            self.tableView.reloadData()
            })
    }
    
    
    
    @IBAction func unwindToVC(segue: UIStoryboardSegue) {
    }
    
}

//
//  SessionText.swift
//  FlairManager
//
//  Created by Greg Mirabito on 10/24/15.
//  Copyright © 2015 Onset. All rights reserved.
//

import UIKit

public class FWSessionText: NSObject, NSCoding {
    public enum TextOriginator:Int{
        case Manager
        case Customer
    }
    
    public enum TextType:Int{
        case HelpRequestSent
        case HelpIsOnTheWayResponse
        case LocationRequest
        case NotEnoughInfoEndingSession
        case LocationResponse
        case SessionTimerExpired
        
    }
    
   public var originator:TextOriginator
   public var date:NSDate!
   public var message:String!
    public var textType:TextType
    
    public init(originator:TextOriginator!, date:NSDate!, message:String!, type:TextType!) {
        self.originator = originator
        self.date = date
        self.message = message
        self.textType = type
    }
    
    public required init?(coder decoder: NSCoder) {
        self.originator = TextOriginator(rawValue: decoder.decodeIntegerForKey("originator"))!
        self.date = decoder.decodeObjectForKey("date") as! NSDate
        self.message = decoder.decodeObjectForKey("message") as! String
        self.textType =  TextType(rawValue: decoder.decodeIntegerForKey("textType"))!
        
    }
    
    public func encodeWithCoder(coder: NSCoder) {
        coder.encodeInteger(self.originator.rawValue, forKey: "originator")
        coder.encodeObject(self.date, forKey: "date")
        coder.encodeObject(self.message, forKey: "message")
        coder.encodeInteger(self.textType.rawValue, forKey: "textType")
    }

}

//
//  FWHelpRequestCodec.swift
//  FWCentral
//
//  Created by Greg Mirabito on 1/19/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import UIKit

class FWHelpRequestCodec: NSObject {
    class func encode(helpRequest:FWHelpRequest)->NSData{
        
        let data = NSMutableData()
        var length = helpRequest.username.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
        data.appendBytes(&length, length: 1)
        data.appendData(helpRequest.username.dataUsingEncoding(NSUTF8StringEncoding)!)
        length = helpRequest.message!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
        data.appendBytes(&length, length: 1)
        data.appendData((helpRequest.message?.dataUsingEncoding(NSUTF8StringEncoding))!)
        if(helpRequest.avatar != nil){
            data.appendData(UIImagePNGRepresentation(helpRequest.avatar!)!)
        }
        
        return data
    }
    
    class func decode(data:NSData)->FWHelpRequest{
        var len:Int = 0
        var index = 0
        
        data.getBytes(&len, range: NSMakeRange(index, 1))
        index++
        
        let usernameData = data.subdataWithRange(NSRange(location: index,length: len))
        let username = NSString(data: usernameData, encoding: NSUTF8StringEncoding)
        
        index += usernameData.length
        data.getBytes(&len, range: NSRange(location: index, length: 1))
        
        index++
        let messageData = data.subdataWithRange(NSRange(location: index, length: len))
        let message = NSString(data: messageData, encoding: NSUTF8StringEncoding)
        
        index += messageData.length
        
        let avatarData = data.subdataWithRange(NSRange(location: index, length: data.length - index))
        let avatar = UIImage(data: avatarData)
        
        let helpRequest = FWHelpRequest(avatar: avatar, message: message as? String, username: username as! String)
        
        return helpRequest
        
    }
}

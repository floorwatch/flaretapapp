//
//  FWClientMessageService.swift
//
//
//  Created by Greg Mirabito on 1/27/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

protocol FWClientMessageService{
    func sendSessionText(central:CBCentral, sessionText:FWSessionText)
}
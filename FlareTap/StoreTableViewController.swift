//
//  NewsTableViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import MapKit

class StoreTableViewController: UITableViewController, RestAPIManagerDelegate, MKMapViewDelegate {
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet var extraButton:UIBarButtonItem!
    let regionRadius: CLLocationDistance = 1000

    //@IBOutlet weak var mapView: MKMapView!
    var stores = [Store]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        extraButton?.enabled      = false
        extraButton?.tintColor    = UIColor.clearColor()
        CozyLoadingActivity.show("Getting Stores in your area...", sender: self, disableUI: true)
        let manager = RestAPIManager(host:"")
        manager.delegate = self
        manager.getStoreList("", parameters:nil)
        print("In did load")

        let initialLocation = CLLocation(latitude: 41.951623, longitude: -70.716443)
        centerMapOnLocation(initialLocation)
        
        //mapView.delegate = self
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = "revealToggle:"
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Store count is: \(stores.count)")
        return stores.count + 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print("Creating cells")
        var cell : UITableViewCell
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("UserItem", forIndexPath: indexPath)
            let userFirstName = cell.viewWithTag(100) as! UILabel
            userFirstName.text = "Ian"
            let timer = cell.viewWithTag(200) as! UILabel
            timer.text = "  5:00"
            let whiteView = cell.viewWithTag(300) as! UIView!
            let path = UIBezierPath(roundedRect:whiteView.bounds, byRoundingCorners:[.TopRight, .TopLeft], cornerRadii: CGSizeMake(80, 80))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.CGPath
            whiteView.layer.mask = maskLayer
            let userImageView = cell.viewWithTag(400) as! UIImageView
            userImageView.contentMode = UIViewContentMode.ScaleAspectFit
            userImageView.image = UIImage(named: "facebookProfilePic")
            userImageView.layer.cornerRadius = (userImageView.frame.size.width / 2)
            userImageView.layer.borderWidth = 7
            userImageView.clipsToBounds = true
            userImageView.layer.borderColor = UIColor.whiteColor().CGColor
            let userLastName = cell.viewWithTag(500) as! UILabel
            userLastName.text = "Jones"
            
        } else {
        cell = tableView.dequeueReusableCellWithIdentifier("StoreItem", forIndexPath: indexPath)
        let store = stores[indexPath.row - 1]
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        configureTextForCell(cell, withStore: store)
        configureImageForCell(cell, withStore: store)
        print("before configure button for cell")
        configureButtonsForCell(cell, withStore: store)
        }
        return cell
    }
    
    
    func configureTextForCell(cell: UITableViewCell, withStore store: Store) {
        let storeName = cell.viewWithTag(1200) as! UILabel
        storeName.text = store.storeName!
        let address1 = cell.viewWithTag(1300) as! UILabel
        address1.text = store.address1
        let address2 = cell.viewWithTag(1400) as! UILabel
        address2.text = store.address2
        let stateZip = cell.viewWithTag(1500) as! UILabel
        stateZip.text = store.state!
        let distance = cell.viewWithTag(1600) as! UILabel
        let formattedString = String(format: "%.3f", store.distance!)
        distance.text = formattedString + " mi."
    }
    
    func configureImageForCell(cell: UITableViewCell, withStore store: Store) {
        let image = cell.viewWithTag(1100) as! UIImageView!
        var imageName = ""
        print("Creating image, store description is: \(store.description)")
        if store.storeName!.hasPrefix("Best Buy") {
            imageName = "bestBuy.jpeg"
        } else if store.storeName!.hasPrefix("DICKS") {
            imageName = "dicksSportingGoods.jpeg"
        } else if store.storeName!.hasPrefix("Macys") {
            imageName = "macys.jpeg"
        }
        print("image name is: \(imageName)")
        image.image = UIImage(named: imageName)
    }
    
    func configureButtonsForCell(cell: UITableViewCell, withStore store: Store) {
        print("configure")
        let flareButton = cell.viewWithTag(1700) as! UIButton
        flareButton.setTitle(setFlareButtonText(store.distance!), forState: .Normal)
        print("flarebutton text is: \(flareButton.titleLabel?.text)")
        let callButton = cell.viewWithTag(1800) as! UIButton
        callButton.addTarget(self, action: "phoneButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        let webButton = cell.viewWithTag(1900) as! UIButton
        webButton.addTarget(self, action: "webButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        let socialButton = cell.viewWithTag(2000) as! UIButton
        socialButton.addTarget(self, action: "socialButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func phoneButtonAction(sender: UIButton!) {
        print("in button action")
        if let phoneCallURL:NSURL = NSURL(string: "tel://(508) 732-3640") {
            print("url")
            let application:UIApplication = UIApplication.sharedApplication()
            print("shared application")
            if (application.canOpenURL(phoneCallURL)) {
                print("if")
                application.openURL(phoneCallURL);
            }
        }
    }
    
    func setFlareButtonText(distance: Double) -> String {
        print(distance)
        if(distance < 0.010) {
            print("return Flare")
            return "Flare"
        } else {
            print("returning distance")
            return String(format: "%.2f", distance)
        }
    }
    
    func webButtonAction(sender: UIButton!) {
        print("in button action")
        if let webURL:NSURL = NSURL(string: "http://www.bestbuy.com") {
           UIApplication.sharedApplication().openURL(webURL)
        }
    }
    
    func socialButtonAction(sender: UIButton!) {
        let screenName =  "BestBuy"
        let appURL = NSURL(string: "twitter://user?screen_name=\(screenName)")!
        let webURL = NSURL(string: "https://twitter.com/\(screenName)")!
        
        let application = UIApplication.sharedApplication()
        
        if application.canOpenURL(appURL) {
            application.openURL(appURL)
        } else {
            application.openURL(webURL)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func restAPIManger(manger: RestAPIManager, didReturnResult result: Any) {
        print("in delegate method")
        stores = result as! [Store]
        stores.sortInPlace({$0.distance < $1.distance})
        print("store size is: \(stores.count)")
        CozyLoadingActivity.hide(success: true, animated: true)
        //mapView.addAnnotations(stores)

        tableView.reloadData()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        //mapView.setRegion(coordinateRegion, animated: true)
    }

}

//
//  FWPeripheralManagerDelegate.swift
//  FWPeripheral
//
//  Created by Greg Mirabito on 12/30/15.
//  Copyright © 2015 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

class FWPeripheralManager: NSObject, CBPeripheralManagerDelegate, FWClientMessageService{
    private var manager : CBPeripheralManager?
    var fwClientMessageReceivedDelegate:FWClientMessageReceivedDelegate?
    
    let serviceUUID  = CBUUID(string: "606B92AA-F293-4F3D-9846-7B9A5965972F")
    let helpRequestCharacteristicUUID = CBUUID(string: "8E17AB57-17A4-4640-82CE-9862C27086B9")
    let storeImageCharacteristicUUID = CBUUID(string: "4014BBC4-7C63-40E8-993F-EC62D5A0E438")
    let storeAssociateInfoCharacteristicUUID = CBUUID(string: "3F740BD0-6085-4403-836D-63FCC3128A63")
    let sessionTextCharacteristicUUID = CBUUID(string:"292DF7B0-9037-4652-BAB6-ED9BDD2C2D36")
    
    var helpRequestCharacteristic : CBMutableCharacteristic!
    var sessionTextCharacteristic : CBMutableCharacteristic!
    var storeImageCharacteristic : CBMutableCharacteristic!
    var storeAssociateInfoCharacteristic : CBMutableCharacteristic!
    var service : CBMutableService!
    var subscribedCentrals = [CBCentral]()
    
    var dataSendIndex:Int = 0
    let MTU = 20
    var sendEOM = false
    var dataToSend:NSData!
    var subscribedCentralToSendData:CBCentral!
    var characteristicForSendData:CBMutableCharacteristic!
    
    var helpRequestData:NSMutableData!
    var sessionTextData:NSMutableData!
    
    override init() {
        super.init()
        self.manager = CBPeripheralManager(delegate: self, queue: nil, options: [CBPeripheralManagerOptionShowPowerAlertKey:NSNumber(bool: true)])
        storeAssociateInfoCharacteristic = CBMutableCharacteristic(type: storeAssociateInfoCharacteristicUUID, properties: [.Read] , value: nil, permissions: [.Readable])
        storeImageCharacteristic = CBMutableCharacteristic(type: storeImageCharacteristicUUID, properties: [.Notify] , value: nil, permissions: [.Readable])
        helpRequestCharacteristic = CBMutableCharacteristic(type: helpRequestCharacteristicUUID, properties: [.Notify, .Read, .Write] , value: nil, permissions: [.Readable, .Writeable])
        sessionTextCharacteristic = CBMutableCharacteristic(type: sessionTextCharacteristicUUID, properties: [.Notify, .Read, .Write], value: nil, permissions: [.Readable, .Writeable])
        service = CBMutableService(type: serviceUUID, primary: true);
        service.characteristics = [helpRequestCharacteristic, storeImageCharacteristic, sessionTextCharacteristic]
    
        
    }
    
    
    
    func peripheralManagerDidStartAdvertising(peripheral: CBPeripheralManager, error: NSError?) {
        if error == nil{
            print("Successfully started advertising")
            
            let message = "The unique identifier of our service is: \(serviceUUID.UUIDString)"
            print(message)
            
            
        } else {
            print("Failed to advertise. Error = \(error)")
        }
        
    }
    
    
    
    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
        switch peripheral.state{
        case .PoweredOn:
            manager?.addService(service)
        
            peripheral.startAdvertising([CBAdvertisementDataLocalNameKey:"Greg M peripheral",
                CBAdvertisementDataServiceUUIDsKey: [serviceUUID]])

        default:
            return;
        }
    }
    
    func peripheralManagerIsReadyToUpdateSubscribers(peripheral: CBPeripheralManager) {
        sendData()
    }
    
    
    
    func peripheralManager(peripheral: CBPeripheralManager, didAddService service: CBService, error: NSError?) {
        if(error != nil){
            print("Error adding service")
        }else{
            print("Service added")
        }
    }
    
    func peripheralManager(peripheral: CBPeripheralManager, didReceiveWriteRequests requests: [CBATTRequest]) {
        
        for request in requests {
            if(request.characteristic.UUID == helpRequestCharacteristicUUID){
                if(isEOM(request.value!)){
                    fwClientMessageReceivedDelegate!.didReceiveHelpRequest(FWHelpRequestCodec.decode(helpRequestData), client: request.central)
                    helpRequestData = nil
                }else{
                    if(helpRequestData == nil){
                        helpRequestData = NSMutableData()
                    }
                    helpRequestData.appendData(request.value!)
                }
                manager?.respondToRequest(request, withResult: CBATTError.Success)
                
                
            }else if(request.characteristic.UUID == sessionTextCharacteristicUUID){
                if(isEOM(request.value!)){
                    fwClientMessageReceivedDelegate!.didReceiveSessionText(FWSessionTextCodec.decode(sessionTextData), client: request.central)
                    sessionTextData = nil
                }else{
                    if(sessionTextData == nil){
                        sessionTextData = NSMutableData()
                    }
                    sessionTextData.appendData(request.value!)
                }
                manager?.respondToRequest(request, withResult: CBATTError.Success)
            }else{
                manager?.respondToRequest(requests[0], withResult: CBATTError.AttributeNotFound)
                return
            }
            
            
        }
    }
    
   
    
    func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didSubscribeToCharacteristic characteristic: CBCharacteristic) {
        print("Subscribed to characteristic updates")
        if(characteristic.UUID == storeImageCharacteristicUUID){
            let image = UIImage(named: "bestBuy.png")
            dataSendIndex = 0
            
            storeImageCharacteristic.value = UIImagePNGRepresentation(image!)
            subscribedCentralToSendData = central
            dataToSend = storeImageCharacteristic.value!
            characteristicForSendData = storeImageCharacteristic
            sendData()
        }else{
            subscribedCentrals.append(central)

        }
    
    }
    
    func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFromCharacteristic characteristic: CBCharacteristic) {
        print("unsubsribed to characteristic")
    }
    
    func peripheralManager(peripheral: CBPeripheralManager, didReceiveReadRequest request: CBATTRequest) {
        switch request.characteristic.UUID{
        case storeAssociateInfoCharacteristic: peripheral.respondToRequest(request, withResult: .ReadNotPermitted)
        default: peripheral.respondToRequest(request, withResult: .ReadNotPermitted)
        }
    }
    
    func isEOM(data:NSData!) ->Bool{
        if(data.length == 3){
            let str = NSString(data: data!, encoding: NSUTF8StringEncoding) as String!
            if(str == "EOM"){
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    func sendData()
    {
        var numBtyesToSend:Int = 0

        var isDone = false
        while(!isDone && !sendEOM){
            numBtyesToSend = min(MTU, self.dataToSend.length - dataSendIndex)
            let range = NSMakeRange(dataSendIndex, numBtyesToSend)
            let dataChunk = self.dataToSend.subdataWithRange(range)
            let didSend = manager!.updateValue(dataChunk, forCharacteristic: self.characteristicForSendData, onSubscribedCentrals: [self.subscribedCentralToSendData])
            if(!didSend){
                // didn't send, so we'll exit and wait for peripheralManagerIsReadyToUpdateSubscribers to call sendData again
                return;
            }else{
                dataSendIndex += numBtyesToSend
                if(dataSendIndex >= dataToSend.length){
                    isDone = true
                    sendEOM = true
                }
            }
        }
        if(sendEOM){
            let eom = "EOM"
            let eomData = eom.dataUsingEncoding(NSUTF8StringEncoding)
            let didSend = manager!.updateValue(eomData!, forCharacteristic: characteristicForSendData, onSubscribedCentrals: [subscribedCentralToSendData])
            if(didSend){
                dataSendIndex = 0
                sendEOM = false
                characteristicForSendData = nil
                dataToSend = nil
                subscribedCentralToSendData = nil
    
            }
        }
    }
    
    
    func sendSessionText(central: CBCentral, sessionText: FWSessionText) {
        let data = FWSessionTextCodec.encode(sessionText)
        //init sendEOM to false
        sendEOM = false
        characteristicForSendData = sessionTextCharacteristic
        dataToSend = data
        subscribedCentralToSendData = central
        sendData()
    }
}

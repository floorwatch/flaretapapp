//
//  MenuController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import CoreData

class MenuController: UITableViewController {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var user: [NSManagedObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("Will Appear")
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "User")
        
        //3
        do {
            print("get results")
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            user = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        } catch {
            print("Some other exception")
        }
        let currentUser: NSManagedObject?
        print("current user count is: \(user.count)")
        if (user.count > 0) {
            currentUser = user[0]
            print (currentUser!.valueForKey("name"))
            userName.text = currentUser!.valueForKey("name") as? String
            print("setting image")
            print("path to picture is: \(currentUser!.valueForKey("pathToPicture"))")
            _ = currentUser!.valueForKey("pathToPicture") as! String
            let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
            let fileURL = documentsURL.URLByAppendingPathComponent("userPic.png")
            let data = NSData(contentsOfURL: fileURL)
            let image = UIImage(data: data!)
            self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
            self.imageView.image = image
//            self.imageView.layer.cornerRadius = (self.imageView.frame.size.width / 2)
//            self.imageView.layer.borderWidth = 5
//            self.imageView.clipsToBounds = true
//            self.imageView.layer.borderColor = UIColor.whiteColor().CGColor
            print("Image set")
        } else {
            userName.text = "Please Log In"
            let image = UIImage(contentsOfFile: "")
            self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
            self.imageView.image = image
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

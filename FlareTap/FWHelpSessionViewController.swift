//
//  FWHelpSessionViewController.swift
//  FloorwatchClient
//
//  Created by Greg Mirabito on 10/7/15.
//  Copyright © 2015 Onset. All rights reserved.
//

import UIKit
import CoreBluetooth



class FWHelpSessionViewController: UIViewController, UITextFieldDelegate, FWStoreAssociateMessageReceivedDelegate {
    
    enum SessionStatus{
        case SessionNotStarted
        case HelpRequestSent
        case HelpRequestReceived
        case HelpIsOnWay
        case LocationInfoRequested
        case LocationReplySent
        case SessionClosedDueToLackOfLocationInfo
        case SessionTimerExpired
        case CustomerCancelledSession
        
    }
    
    
    var peripheral:CBPeripheral?
    var centralManager:FWCentralManager?
    var isHelpReqestSent:Bool = false
    var isCancelRequestSent:Bool = false
    var helpRequest:FWHelpRequest?
    
    var sessionTexts = [FWSessionText]()
    var sessionStatus = SessionStatus.SessionNotStarted
    var timer:NSTimer?
    let responseTimeLimitSeconds:Int = 5*60
    var timerStartDate:NSDate?
    
    
    @IBOutlet weak var sendLocationButton: UIButton!
    @IBOutlet weak var sessionTextsLabel: UILabel!
    @IBOutlet weak var helpSubjectTextField: UITextField!
    @IBOutlet weak var sendFlareButton: UIButton!
    
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sessionTextsLabel.lineBreakMode = .ByWordWrapping
        sessionTextsLabel.numberOfLines = 0
        sessionTextsLabel.text = ""
        sendLocationButton.hidden = true
        timerLabel.hidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func sendFlareButtonPressed(sender: AnyObject) {
        let location = locationTextField.text
        if(location!.isEmpty){
            let alert = UIAlertController(title: "Location Required", message: "You must provide your location within the store", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okAction)
            self.showViewController(alert, sender: self)
        }else{
            self.view.endEditing(true)
            self.startHelpSession()
        }
    }
    
    @IBAction func sendLocationButtonPressed(sender: UIButton) {
        let location = locationTextField.text
        if(location!.isEmpty){
            let alert = UIAlertController(title: "Location Required", message: "You must provide your location within the store", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okAction)
            self.showViewController(alert, sender: self)
        }else{
            self.view.endEditing(true)
            self.sendLocationInfo()
        }
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        //todo: ask if customer got help they needed, cancel session
        sessionStatus = .CustomerCancelledSession
        //todo: send cancelled by customer message to manager so manager knows to clear session
        centralManager?.disconnect(peripheral!)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func sendLocationInfo(){
        //todo: send in thread
        let locationString = "I am located " + locationTextField.text! + "."
        
        let locationMessage = FWSessionText(originator: .Customer, date: NSDate(), message: locationString, type: FWSessionText.TextType.LocationResponse)
        centralManager?.sendSessionText(peripheral!, sessionText: locationMessage)
        sessionStatus = .LocationReplySent
        sessionTexts.append(locationMessage)
        refreshSessionTextsLabel()
        sendLocationButton.hidden = true
        helpLabel.hidden = true
        
        
    }
    //invites remote peer to session (sending a new help request)
    func startHelpSession(){
         //todo: send this in a separate thread and put up a wait spinner to block UI until response is received
            let requestDate = NSDate()
            print("requestDate: \(requestDate)")
            var helpText = "I am located " + locationTextField.text! + "."
            if (!helpSubjectTextField.text!.isEmpty) {
                helpText += " I need help with " + helpSubjectTextField.text!
            }
            helpRequest = FWHelpRequest(avatar: UIImage(named: "greg.png"), message:helpText, username: "greg(todo:replace)")
            centralManager?.createNewHelpRequest(peripheral!, helpRequest: helpRequest!)
            sessionStatus = .HelpRequestSent
            sessionTexts.append(FWSessionText(originator: .Customer, date: requestDate, message: helpText, type:.HelpRequestSent))
            refreshSessionTextsLabel()
            sendFlareButton.hidden = true
    }
    
    func refreshSessionTextsLabel(){
        var text = ""
        let dateFmt = NSDateFormatter()
        dateFmt.dateStyle = .NoStyle
        dateFmt.timeStyle = .MediumStyle
        
        
        for sessionText in sessionTexts{
            if text.characters.count > 0 {
                text += "\n\n"
            }
            let textOriginator = sessionText.originator == FWSessionText.TextOriginator.Manager ? "Manager" : "Customer"
            text += ("\(dateFmt.stringFromDate(sessionText.date)) - \(textOriginator): \(sessionText.message)")
            print("text date: \(sessionText.date)")
            print("formatted text date: \(dateFmt.stringFromDate(sessionText.date))")
        }
        sessionTextsLabel.text = text
    }
    
   
    
    

    
    func calculateResponseTimeSecsRemaining()->Int{
        
        let elapsedTime:Int = Int(abs(timerStartDate!.timeIntervalSinceNow))
        var responseTimeRemaining = responseTimeLimitSeconds - Int(elapsedTime)
        if responseTimeRemaining <= 0 {
            responseTimeRemaining = 0
        }
        return responseTimeRemaining
    }
    
    
    func sessionTimerFired(){
        let responseTimeRemaining = calculateResponseTimeSecsRemaining()
        let minutes = Int(responseTimeRemaining/60)
        let seconds = Int(responseTimeRemaining%60)
        let remainingTimeStr = String(format: "%02d:%02d", minutes, seconds)
        timerLabel.text = remainingTimeStr
        timerLabel.hidden = false
    }
    
    func startSessionTimer(){
        if((self.timer) != nil){
            timer?.invalidate()
        }
        self.timerStartDate = NSDate()
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "sessionTimerFired", userInfo: nil, repeats: true)
        
        
    }
    
    func cancelSessionTimer(){
        if(self.timer != nil){
            self.timer?.invalidate()
        }
        self.timerLabel.hidden=true
    }
    

    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func didReceiveHelpRequestAcknowlegement(peripheral: CBPeripheral) {
         //todo take down wait spinner and allow UI activity to continue
        self.sessionStatus = SessionStatus.HelpRequestReceived
        self.sendLocationButton.hidden = true
        self.helpSubjectTextField.hidden = true
        self.startSessionTimer()
        
    }
    
    //this is called when we get an ack from the manager app that it received the last session text message we sent
    func didReceiveSessionTextAcknowlegement(peripgeral: CBPeripheral) {
       //todo take down wait spinner and allow UI activity to continue
    }
    
    
    //This is called when we receive a session text from the manager
    func didReceiveSessionText(text: FWSessionText, peripheral: CBPeripheral) {
        if(text.textType == .LocationRequest){
            self.sessionStatus = SessionStatus.LocationInfoRequested
            self.sendLocationButton.hidden = false
            self.helpSubjectTextField.hidden = true
            self.sendFlareButton.hidden = true
            self.startSessionTimer()
        }else if text.textType == .HelpIsOnTheWayResponse{
            self.sessionStatus = SessionStatus.HelpIsOnWay
            self.sendLocationButton.hidden = true
            self.startSessionTimer()
        }else if text.textType == .NotEnoughInfoEndingSession{
            self.sessionStatus = SessionStatus.SessionClosedDueToLackOfLocationInfo
            self.cancelSessionTimer()
        }else if text.textType == .SessionTimerExpired {
            self.sessionStatus = .SessionTimerExpired
            self.cancelSessionTimer()
            centralManager?.disconnect(peripheral)
            //todo: handle expired session actions
            
        }else{
            //throw exception here, text status is invalid
        }
     

        sessionTexts.append(text)
        refreshSessionTextsLabel()
    }
    
    func didDisconnect(peripheral: CBPeripheral) {
        self.cancelSessionTimer()
        var alert:UIAlertController;
        if self.sessionStatus == .HelpRequestSent {
            //show alert that help request was unsucessful, please try again.
            alert = UIAlertController(title: "Request Unsucessful", message: "The help request could not be sent. Please try again.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }))
        }else if self.sessionStatus == .SessionClosedDueToLackOfLocationInfo{
            
            //Manager closed help request due to lack of location info
            alert = UIAlertController(title: "Help Request Closed", message: "The request was closed because the manager could not locate you. If you still need help please start a new help request.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }))
        }else if(self.sessionStatus == .SessionTimerExpired){
            alert = UIAlertController(title: "Help Request Expired", message: "The help request has expired. Did you get the help you needed?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                //self.dismissViewControllerAnimated(true, completion: nil)
                //allow customer to rate service
                self.performSegueWithIdentifier("ratingSegue", sender: self)
                
                
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }))
        }else{
            //show alert that connection was lost during session, please try again.
            alert = UIAlertController(title: "Connection Lost", message: "The connection was unexpectedly lost. Please try again.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }))
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}

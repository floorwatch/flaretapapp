//
//  FWSessionTextCodec.swift
//  FWCentral
//
//  Created by Greg Mirabito on 1/17/16.
//  Copyright © 2016 Onset. All rights reserved.
//
//  This class is used to encode and decode FWSessionText objects for transmission over core bluetooth

import UIKit

class FWSessionTextCodec: NSObject {
   //Encodes an FWSessionText object into NSData to send over core bluetooth
   class func encode(sessionText:FWSessionText!) -> NSData{
        let data = NSMutableData()
        var textType:Int = sessionText.textType.rawValue
        var originator:Int = sessionText.originator.rawValue
        var date:Double = sessionText.date.timeIntervalSinceReferenceDate as Double
        let message:NSString = sessionText.message!
        data.appendBytes(&textType, length: 1)
        data.appendBytes(&originator, length: 1)
        data.appendBytes(&date, length: sizeofValue(date))
        data.appendData(message.dataUsingEncoding(NSUTF8StringEncoding)!)
        return data
    }
    
   //Decodes an NSData object received from core bluetooth into an FWSessionTextObject
   class func decode(data:NSData!) ->FWSessionText{
        var textType:Int = 0
        var originator:Int = 0
        var date:Double = 0
        var message:String = ""

        data.getBytes(&textType,length: 1)
        
        var range = NSMakeRange(1, 1)
        
        data.getBytes(&originator, range:range)
        range = NSMakeRange(2, 8)
        data.getBytes(&date, range:range)
        range = NSMakeRange(10, data.length - 10)
        let messageData = data.subdataWithRange(range)
    
        message = NSString(data:messageData, encoding:NSUTF8StringEncoding) as! String
        let originatorEnum = FWSessionText.TextOriginator(rawValue: originator)
        let textTypeEnum = FWSessionText.TextType(rawValue: textType)
    
        return FWSessionText(originator: originatorEnum, date: NSDate(timeIntervalSinceReferenceDate: date), message: message, type: textTypeEnum)
        
    }
}

//
//  FWStoreAssociateCell.swift
//  FWCentral
//
//  Created by Greg Mirabito on 1/10/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import UIKit

class FWStoreAssociateCell: UITableViewCell {
    var advertisement:FWStoreAssociateAdvertisement! {
        willSet(newAdvertisement){
            signalStrengthLabel.text = "Signal Strength: " + newAdvertisement.RSSI.stringValue
            storeImageView.image = newAdvertisement.storeImage
            storeNameLabel.text = newAdvertisement.storeName
        }
    }
    var sessionMessagaes = [FWSessionText]()
    @IBOutlet weak var signalStrengthLabel: UILabel!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var storeImageView: UIImageView!
  
}

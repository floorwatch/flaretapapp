//
//  FWStoreAssociateMessageService.swift
//  FWCentral
//
//  Created by Greg Mirabito on 1/27/16.
//  Copyright © 2016 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

protocol FWStoreAssociateMessageService{
    func createNewHelpRequest(peripheral:CBPeripheral, helpRequest:FWHelpRequest)
    func sendSessionText(peripheral:CBPeripheral, sessionText:FWSessionText)
    func canceHelpRequest(peripheral:CBPeripheral)
}
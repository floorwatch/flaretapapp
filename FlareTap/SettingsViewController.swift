//
//  PhotoViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import CoreData

class SettingsViewController: UIViewController, FBSDKLoginButtonDelegate {
    @IBOutlet weak var menuButton:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = "revealToggle:"
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            if (FBSDKAccessToken.currentAccessToken() != nil) {
                // User is already logged in, do work such as go to next view controller.
                print("Not nil")
                let loginView : FBSDKLoginButton = FBSDKLoginButton()
                self.view.addSubview(loginView)
                loginView.center = self.view.center
                loginView.delegate = self

            } else {
                let loginView : FBSDKLoginButton = FBSDKLoginButton()
                self.view.addSubview(loginView)
                loginView.center = self.view.center
                loginView.readPermissions = ["public_profile", "email", "user_friends"]
                loginView.delegate = self
            }
        }
    }
    
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        // Facebook Delegate Methods
        
        func loginButton(loginButton: FBSDKLoginButton!,
            didCompleteWithResult result: FBSDKLoginManagerLoginResult!,
            error: NSError!) {
                print("User Logged In")
                
                if ((error) != nil)
                {
                    // Process error
                } else if result.isCancelled {
                    // Handle cancellations
                } else {
                    // If you ask for multiple permissions at once, you
                    // should check if specific permissions missing
                    if result.grantedPermissions.contains("email")
                    {
                        // Do work
                    }
                    returnUserData()
                }
        }
        
        func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
            clearCoreData("User")
            print("User Logged Out")
        }
        
    func returnUserData() {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,id"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil) {
                // Process error
                print("Error: \(error)")
            } else {
                
                print("fetched user: \(result)")
                let userName : NSString = result.valueForKey("name") as! NSString
                print("User Name is: \(userName)")
                let userEmail : NSString = result.valueForKey("email") as! NSString
                print("User Email is: \(userEmail)")
                let userId : NSString = result.valueForKey("id") as! NSString
                print("user ID is: \(userId)")
                let userProfile = "https://graph.facebook.com/" +  (userId as String) + "/picture?type=large"
                print("userProfile is: \(userProfile)")
                let profilePictureUrl = NSURL(string: userProfile)
                
                let profilePictureData = NSData(contentsOfURL: profilePictureUrl!)
                
                print("Have profile picture")
                if(profilePictureData != nil) {
                    print("trying to save data")
                        let filePath = self.writeImageToFileSystem(profilePictureData!, userID: userId as String)
                        print("File path is: " + filePath!)
                        self.saveUserData(userName as String, imagePath: "\(userId).png", id: userId as String, email: userEmail as String)
                    
                }
                
            }
        })
        
    }
    
    func saveUserData (userName: String, imagePath: String, id: String, email: String) {
        checkForUser(userName)
        
        print("App delegate")
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        print("Managed Context")

        let managedContext = appDelegate.managedObjectContext
        
        print("Setting values")
        let user = NSEntityDescription.insertNewObjectForEntityForName("User",
            inManagedObjectContext: managedContext) as! User
        user.name = userName
        user.pathToPicture = imagePath
        user.facebookID = id
        user.email = email
        //4
        do {
            print("Trying Save")
            try managedContext.save()
            print("Save worked")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func clearCoreData(entity:String) {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest()
        fetchRequest.entity = NSEntityDescription.entityForName(entity, inManagedObjectContext: managedContext)
        fetchRequest.includesPropertyValues = false
        do {
            if let results = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
                for result in results {
                    managedContext.deleteObject(result)
                }
                
                try managedContext.save()
            }
        } catch {
            print("failed to clear core data")
        }
    }
    
    func checkForUser(userName: String) {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "User")
        
        var users: [NSManagedObject]
        
        //3
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            users = results as! [NSManagedObject]
            
            for user in users {
                if (user.valueForKey("name") as! String == userName) {
                    print("All ready in DB")
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        } catch {
            print("Some other exception")
        }
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        print("documentsDirectory: \(documentsDirectory)")
        return documentsDirectory
    }
    
    func writeImageToFileSystem(fileData: NSData, userID: String)->String? {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
        if let image = UIImage(data: fileData) {
            let fileName = "userPic.png"
            let fileURL = documentsURL.URLByAppendingPathComponent(fileName)
            if let pngImageData = UIImagePNGRepresentation(image) {
                pngImageData.writeToURL(fileURL, atomically: false)
            }
            return self.getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        }
        return nil
    }
}

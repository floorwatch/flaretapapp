//
//  FWCentralManager.swift
//  FWCentral
//
//  Created by Greg Mirabito on 12/30/15.
//  Copyright © 2015 Onset. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

class FWCentralManager:NSObject, CBCentralManagerDelegate, CBPeripheralDelegate, FWStoreAssociateMessageService{
    enum Operation{
        case None
        case CreateHelpRequst
        case SendSessionText
        case GetStoreInfo
        case GetStoreImage
        case ConnectForClient
    }
    let MTU = 512
    private var manager : CBCentralManager!
    var peripherals = [CBPeripheral]()
    var fwStoreAssociateListener:FWStoreAssociateListener?
    var fwStoreAssociateMessageReceivedDelegate:FWStoreAssociateMessageReceivedDelegate?
    let serviceUUID  = CBUUID(string: "606B92AA-F293-4F3D-9846-7B9A5965972F")
    let helpRequestCharacteristicUUID = CBUUID(string: "8E17AB57-17A4-4640-82CE-9862C27086B9")
    let storeImageCharacteristicUUID = CBUUID(string: "4014BBC4-7C63-40E8-993F-EC62D5A0E438")
    let storeAssociateInfoCharacteristicUUID = CBUUID(string: "3F740BD0-6085-4403-836D-63FCC3128A63")
    let sessionTextCharacteristicUUID = CBUUID(string:"292DF7B0-9037-4652-BAB6-ED9BDD2C2D36")
    
    var helpRequestCharacteristic:CBCharacteristic?
    var sessionTextCharacteristic : CBCharacteristic?
    var storeImageCharacteristic:CBCharacteristic?
    var storeAssociateInfoCharacteristic:CBCharacteristic?
    
    var storeImageData:NSMutableData = NSMutableData()
    var sessionTextData:NSMutableData = NSMutableData()
    
    var currentPeripheral:CBPeripheral?
    var currentOperation:Operation = .None
    var operationQueue:[Operation] = Array<Operation>()
    
    var dataSendIndex:Int = 0
    
    var sendEOM = false
    var dataToSend:NSData!
    var peripheralToSendData:CBPeripheral!
    var characteristicForSendData:CBCharacteristic!
    var numDataPacketsSent:Int = 0
    var numDataPacketsAcknowleged:Int = 0
    
    var isConnectionForClient:Bool = false
    var helpRequest:FWHelpRequest?
    var sessionText:FWSessionText?
    
    
    init(storeAssociateListener:FWStoreAssociateListener) {
        super.init()
        manager = CBCentralManager(delegate: self, queue: nil, options: nil)
        fwStoreAssociateListener = storeAssociateListener;
        
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        print("Connected")
        peripheral.delegate = self
        peripheral.discoverServices([serviceUUID])
    }
    
    //connection created and all services/characteristics are ready for service
    func connectionEstablished(peripheral:CBPeripheral){
        if(isConnectionForClient == true){
            //client requested the connection rather than the advertisement process (looking for store info)
            fwStoreAssociateListener?.didConnectToStoreAssociate(peripheral)
            peripheral.setNotifyValue(true, forCharacteristic: sessionTextCharacteristic!)
            
        }else{
            processOperations(peripheral)
        }
    }
    
    
    //process any pending operations to perform on peripheral
    func processOperations(peripheral:CBPeripheral){
        if(operationQueue.count > 0){
            currentOperation = operationQueue.removeLast()
        }else{
            currentOperation = .None
            currentPeripheral = nil
        }
        switch(currentOperation){
        case .None: return
        case .GetStoreInfo: processOperations(peripheral)
            //peripheral.setNotifyValue(true, forCharacteristic: storeAssociateInfoCharacteristic!)
        case .GetStoreImage:
            peripheral.setNotifyValue(true, forCharacteristic: storeImageCharacteristic!)
        case .SendSessionText:
            dataToSend = FWSessionTextCodec.encode(sessionText!)
            peripheralToSendData = peripheral
            characteristicForSendData = sessionTextCharacteristic
            sendData()
        case .CreateHelpRequst:
            //send in chunks
            dataToSend = FWHelpRequestCodec.encode(helpRequest!)
            peripheralToSendData = peripheral
            characteristicForSendData = helpRequestCharacteristic
            sendData()
        case .ConnectForClient:
            fwStoreAssociateListener?.didConnectToStoreAssociate(peripheral)
            peripheral.setNotifyValue(true, forCharacteristic: sessionTextCharacteristic!)
        }
        
    }
    
    
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("Connection failed")
        if(isConnectionForClient == true){
            fwStoreAssociateListener?.didFailToConnectToStoreAssociate()
        }
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        if(error != nil){
           print("Disconnected due to error \(error)")
        }else{
            print("Disconnected")
        }
        fwStoreAssociateMessageReceivedDelegate?.didDisconnect(peripheral)
       
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        switch central.state{
        case .PoweredOn:
            manager.scanForPeripheralsWithServices([serviceUUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey:NSNumber(bool: true)])
        default:
            return
        }
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        if(currentPeripheral != nil && currentPeripheral == peripheral){
            return;
        }
       
        let advertisement = FWStoreAssociateAdvertisement(peripheral: peripheral, storeName: "test", RSSI: RSSI)
        
        //look for saved data for this peripheral in user defaults
        let userDefaults = NSUserDefaults.standardUserDefaults()
               let peripheralDict = userDefaults.dictionaryForKey(peripheral.identifier.UUIDString)
        if(peripheralDict == nil || peripheralDict!["storeImageData"] == nil){
             currentPeripheral = peripheral
            //we need to get info from peripheral because it is not in user defaults
            
            if(operationQueue.count == 0){
                //only do if nothing is on queue
                operationQueue.insert(.GetStoreImage, atIndex: 0)
               // operationQueue.insert(.GetStoreInfo, atIndex: 0)
                isConnectionForClient = false
                manager.connectPeripheral(peripheral, options: nil)
            }
        }else{
            let storeName = peripheralDict!["storeName"] as? NSString
            advertisement.storeName = storeName as? String
            let imageData = peripheralDict!["storeImageData"] as? NSData
            if(imageData != nil){
                advertisement.storeImage = UIImage(data: imageData!)
            }
            fwStoreAssociateListener?.didReceiveAdvertisement(advertisement)
        }
      

    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
       let service = peripheral.services![0]
       peripheral.discoverCharacteristics([helpRequestCharacteristicUUID, storeAssociateInfoCharacteristicUUID, storeImageCharacteristicUUID, sessionTextCharacteristicUUID], forService: service)
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        
        for characteristic in service.characteristics! {
            switch characteristic.UUID{
            case helpRequestCharacteristicUUID:
                helpRequestCharacteristic = characteristic
            case storeImageCharacteristicUUID:
                storeImageCharacteristic = characteristic
            case storeAssociateInfoCharacteristicUUID:
                storeAssociateInfoCharacteristic = characteristic
            case sessionTextCharacteristicUUID:
                sessionTextCharacteristic = characteristic
            default: return
            }
           
        }
         connectionEstablished(peripheral)
        
        
        
        
    }
    
    func peripheral(peripheral: CBPeripheral, didWriteValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        numDataPacketsAcknowleged++
        if(error != nil){
            if(currentOperation == .CreateHelpRequst){
                print("Error creating help request")
            }else if(currentOperation == .SendSessionText){
                 print("Error sending help request")
            }else{
                print("Error writing characteristic")
            }
        }else if(currentOperation == .CreateHelpRequst){
            if(numDataPacketsSent == numDataPacketsAcknowleged){
               print("helpRequest Received By Peripheral")
               fwStoreAssociateMessageReceivedDelegate?.didReceiveHelpRequestAcknowlegement(peripheral)
            }else{
                print("Received ack for help request data chunk")
            }
        }else if(currentOperation == .SendSessionText){
            if(numDataPacketsSent == numDataPacketsAcknowleged){
                fwStoreAssociateMessageReceivedDelegate?.didReceiveSessionTextAcknowlegement(peripheral)
            }else{
                print("Received ack for session text data chunk")
            }
        }else{
            print("Write success")
        }
        
    }
    


    func peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if(error != nil){
            print("Error subscribing to characteristis updates")
        }else{
            print("Successfully subscribed to notifications for characteristic changes")
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if(error != nil){
            print("Error updating characteristic value")
        }else if(characteristic.UUID == storeImageCharacteristicUUID){
            if(isEOM(characteristic.value!)){
                storeImageDataReceived(peripheral, imageData:storeImageData)
                peripheral.setNotifyValue(false, forCharacteristic: characteristic)
            }else{
                storeImageData.appendData(characteristic.value!)
            }
    
        }else if(characteristic.UUID == sessionTextCharacteristicUUID){
            if(isEOM(characteristic.value!)){
               sessionTextRecevied(peripheral, sessionTextData: sessionTextData)
               //reset text data
               sessionTextData = NSMutableData()
            }else{
               sessionTextData.appendData(characteristic.value!)
            }
        }
    }
    
    func sessionTextRecevied(peripheral:CBPeripheral, sessionTextData:NSData){
        let sessionText = FWSessionTextCodec.decode(sessionTextData)
        fwStoreAssociateMessageReceivedDelegate?.didReceiveSessionText(sessionText, peripheral: peripheral)
    }
    
    func storeImageDataReceived(peripheral:CBPeripheral, imageData:NSData){
        let peripheralInfoDict = NSUserDefaults.standardUserDefaults().dictionaryForKey(peripheral.identifier.UUIDString)
        var newDict: NSMutableDictionary
        if(peripheralInfoDict == nil){
            newDict = NSMutableDictionary()
        }else{
            let oldDict = peripheralInfoDict! as NSDictionary
            newDict = oldDict.mutableCopy() as! NSMutableDictionary
        }
         newDict.setObject(imageData, forKey: "storeImageData")
         NSUserDefaults.standardUserDefaults().setObject(newDict, forKey: peripheral.identifier.UUIDString)
         processOperations(peripheral)
    }
    
    func isEOM(data:NSData!) ->Bool{
        if(data.length == 3){
            let str = NSString(data: data!, encoding: NSUTF8StringEncoding) as String!
            if(str == "EOM"){
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    
   
   

    func sendData()
    {
        var numBtyesToSend:Int = 0
        numDataPacketsAcknowleged = 0
        numDataPacketsSent = 0
        var isDone = false
        while(!isDone && !sendEOM){
            numBtyesToSend = min(MTU, self.dataToSend.length - dataSendIndex)
            let range = NSMakeRange(dataSendIndex, numBtyesToSend)
            let dataChunk = self.dataToSend.subdataWithRange(range)
            numDataPacketsSent++
            peripheralToSendData.writeValue(dataChunk, forCharacteristic: characteristicForSendData, type: .WithResponse)
            dataSendIndex += numBtyesToSend
            if(dataSendIndex >= dataToSend.length){
                isDone = true
                sendEOM = true
            }
        }
        
        if(sendEOM == true){
            let eom = "EOM"
            let eomData = eom.dataUsingEncoding(NSUTF8StringEncoding)
            numDataPacketsSent++
            peripheralToSendData.writeValue(eomData!, forCharacteristic: characteristicForSendData, type: .WithResponse)
            dataSendIndex = 0
            sendEOM = false
            characteristicForSendData = nil
            dataToSend = nil
            peripheralToSendData = nil
            
            
        }
    }
    
    

    func createNewHelpRequest(peripheral:CBPeripheral, helpRequest:FWHelpRequest){
        self.helpRequest = helpRequest
        operationQueue.append(Operation.CreateHelpRequst)
        processOperations(peripheral)
        
    }
    
    
    func startScanningForStoreManagers() {
        manager.scanForPeripheralsWithServices([serviceUUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey:NSNumber(bool: true)])
    }
    
    func stopScanningForStoreManagers(){
        manager.stopScan()
    }
    
    func connectToStoreManager(peripheral: CBPeripheral) {
        isConnectionForClient = true
        manager.connectPeripheral(peripheral, options: nil)
    }
    
    func sendSessionText(peripheral: CBPeripheral, sessionText: FWSessionText) {
        self.sessionText = sessionText
        operationQueue.append(Operation.SendSessionText)
        processOperations(peripheral)
    }
    
    func canceHelpRequest(peripheral: CBPeripheral) {
        //todo
        peripheral.setNotifyValue(false, forCharacteristic: sessionTextCharacteristic!)
    }
    
    func disconnect(peripheral:CBPeripheral){
        peripheral.setNotifyValue(false, forCharacteristic: sessionTextCharacteristic!)
        manager.cancelPeripheralConnection(peripheral)
    }
    
    
    
    
}

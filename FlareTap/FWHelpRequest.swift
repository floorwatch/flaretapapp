//
//  FWHelpRequest.swift
//  FWFlareTapKit
//
//  Created by Greg Mirabito on 11/5/15.
//  Copyright © 2015 FloorWatch. All rights reserved.
//

import UIKit

public class FWHelpRequest: NSObject, NSCoding {
    public var avatar:UIImage?
    public var message:String?
    public var username:String!
    
    public init(avatar:UIImage?, message:String?, username:String) {
        self.avatar = avatar
        self.message = message
        self.username = username
    }
    
    public required init?(coder decoder: NSCoder) {
        self.avatar = decoder.decodeObjectForKey("avatar") as? UIImage
        self.message = decoder.decodeObjectForKey("message") as? String
        self.username = decoder.decodeObjectForKey("username") as! String
        
    }
    
    public func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.avatar, forKey: "avatar")
        coder.encodeObject(self.message, forKey: "message")
        coder.encodeObject(self.username, forKey: "username")
    }
    
    
    
}

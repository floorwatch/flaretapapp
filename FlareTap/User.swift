//
//  User.swift
//  SidebarMenu
//
//  Created by ijones on 11/4/15.
//  Copyright © 2015 AppCoda. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject {
    @NSManaged var name: String
    @NSManaged var pathToPicture: String
    @NSManaged var facebookID: String
    @NSManaged var email: String
}
